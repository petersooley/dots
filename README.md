# Dots

Repository for my dotfiles and other configuration utilities. Also holds install scripts for all my software.

### Structure

This is a bare git repository where the tracked files are stored in `$HOME` while the `.git` folder is stored as `$HOME/.dots`.  Then we use an alias to manage this git repository so we don't have to worry too much about using appropriate flags when using git. 

[This technique is detailed here.](https://www.atlassian.com/git/tutorials/dotfiles)

We're staying pretty loose with configuration management. It's a lot of overhead for little payoff.

### Pre-requisites

This project is currently focused on Ubuntu, for now.

### Start a new system

No need to log in to gitlab, just start with cloning the public repository.

```
git clone --bare https://gitlab.com/petersooley/dots.git ~/.dots
alias dots='/usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME'
dots config --local status.showUntrackedFiles no
dots checkout
dots remote set-url origin git@gitlab.com:petersooley/dots.git
./install.sh
cd .ssh
make unlock       # requires auth pass
```

### Verify installation

```
$ ssh -T git@gitlab.com
```



