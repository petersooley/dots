
function fish_greeting
end

if status is-interactive
    source ("/usr/local/bin/starship" init fish --print-full-init | psub)
    neofetch
end

abbr -a g git
abbr -a d docker
abbr -a dc docker compose

alias dots="/usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME"

alias ls='exa -al --color=always --group-directories-first --icons' # preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing

export EDITOR=nvim
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export UID=(id -u)
export GID=(id -g)
set PATH /home/peter/.cargo/bin $PATH
set PATH /usr/local/go/bin $PATH
