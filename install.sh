#!/usr/bin/env bash

systemctl enable --user --now ~/.config/systemd/user/ssh-agent.service
systemctl enable --user --now ~/.config/systemd/user/bg-changer.timer

UPDATED=false

function apt_update () {
  if [ "$UPDATED" = true ]
  then
    sudo apt update
    UPDATED=true
  fi
}

function apt_install () {
  if dpkg -s $@ &> /dev/null
  then
    echo "$@ is already installed"	  
    return 1
  else
    apt_update
    sudo apt install -y $@
    return 0
  fi
}

apt_install docker docker-compose
apt_install neofetch
apt_install neovim
apt_install exa
apt_install alacritty
apt_install flameshot
apt_install gnome-tweaks
apt_install gnome-shell-extensions
apt_install telegram-desktop
apt_install obs-studio
apt_install vlc
apt_install steam
apt_install htop
apt_install libssl-dev
apt_install build-essential git cmake
apt_install libhidapi-dev
apt_install pavucontrol


sudo groupadd docker
sudo usermod -aG docker $USER

if apt_install fish
then
  echo "Setting default shell to fish"
  chsh -s /usr/bin/fish $USER
fi

if ! command -v starship &> /dev/null
then
	sh -c "$(curl -fsSL https://starship.rs/install.sh)" -- -y
fi

if ! command -v cargo &> /dev/null
then
	sh -c "$(curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs)" -- -y
	cargo install cargo-release
fi

if ! dpkg -s enpass &> /dev/null
then
	echo "deb https://apt.enpass.io/ stable main" | sudo tee /etc/apt/sources.list.d/enpass.list
	wget -O - https://apt.enpass.io/keys/enpass-linux.key | sudo tee /etc/apt/trusted.gpg.d/enpass.asc
	sudo apt update
	sudo apt install -y enpass
fi

if ! command -v typora &> /dev/null
then
	wget -qO - https://typora.io/linux/public-key.asc | sudo tee /etc/apt/trusted.gpg.d/typora.asc
	sudo add-apt-repository -y 'deb https://typora.io/linux ./'
	sudo apt update
	sudo apt install -y typora pandoc
fi

#if [ ! -f "$HOME/.local/share/fonts/NerdFonts/Hack Regular Nerd Font Complete.ttf" ]
#then
#	echo "installing nerdfonts"
#	mkdir -p tmp
#	git clone  --filter=blob:none --sparse https://github.com/ryanoasis/nerd-fonts.git tmp/nerd-fonts
#	cd tmp/nerd-fonts
#	pushd tmp/nerd-fonts
#	git sparse-checkout add patched-fonts/Hack
#	./install.sh Hack
#	popd
#	rm -rf nerd-fonts
#fi

if ! command -v headsetcontrol &> /dev/null
then
	echo "installing headsetcontrol"
	mkdir -p tmp
	git clone https://github.com/Sapd/HeadsetControl tmp/HeadsetControl
	mkdir tmp/HeadsetControl/build
	pushd tmp/HeadsetControl/build
	cmake .. 
	make
	sudo make install
	popd
	rm -rf tmp/HeadsetControl
	sudo udevadm control --reload-rules && sudo udevadm trigger
fi

if ! command -v code &> /dev/null
then
	wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
	sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
	sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
	rm -f packages.microsoft.gpg
	sudo apt update
	sudo apt install -y code
fi
