#!/usr/bin/env bash

# Changes the desktop background to a random file in the ~/.config/backgrounds directory

ls ~/.config/backgrounds | sort -R | tail -n 1 | while read file; do
    gsettings set org.gnome.desktop.background picture-uri "file:///home/peter/.config/backgrounds/$file"
done
